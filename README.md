# Recycler View Demo (Kotlin)

This repo showcases using the recycler view in Kotlin to build a scrollable image and text view:

![Demo](assets/demo.gif)


## Setup

Clone the repo:

```shell
workspace$: git clone https://github.com/Apolexian/Recycler-View-Example.git
```

The example uses hard coded data located in the **DataSource** class. In order to use data from a database, extra logic has to be done to load the data from the source, after which the rest of the code is the same.
