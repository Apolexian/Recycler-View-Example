package com.apolexian.recylerviewexample

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apolexian.recylerviewexample.models.Card
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_card.view.*

class CardRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items: List<Card> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CardViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_card, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CardViewHolder -> {
                holder.bindCard(items[position])
            }
        }
    }

    /** Sets the list of cards for the view*/
    fun submitCardList(cardList: List<Card>) {
        items = cardList
    }

    /**
     * CardViewHolder describes how the view looks like in the recycler view. Takes data from
     * source and sets it to the view. Extends the generic View Holder to match the adapter. Uses
     * glide in order to set the [cardImage]. For more on glide see:
     * https://bumptech.github.io/glide/doc/generatedapi.html
     */
    class CardViewHolder constructor(
        itemView: View

    ) : RecyclerView.ViewHolder(itemView) {
        val cardImage: ImageView = itemView.card_image
        val cardTitle: TextView = itemView.card_title
        val cardAuthor: TextView = itemView.card_author

        fun bindCard(card: Card) {


            val reqOptions = RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)

            Glide.with(itemView.context)
                .applyDefaultRequestOptions(reqOptions)
                .load(card.imgUrl)
                .into(cardImage)

            cardTitle.text = card.titleText
            cardAuthor.text = card.username
        }
    }
}