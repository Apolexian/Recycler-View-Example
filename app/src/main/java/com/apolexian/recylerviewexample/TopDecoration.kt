package com.apolexian.recylerviewexample

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/** Simple item decoration to apply padding to the top of the cards */
class TopDecoration(private val padding: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.top = padding
    }
}