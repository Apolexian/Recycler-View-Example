package com.apolexian.recylerviewexample

import com.apolexian.recylerviewexample.models.Card

/**
 * DataSource is used to mock an actual data source (such as a database) and store dummy data for
 * the cards that will be displayed. The data was acquired from https://www.reddit.com/r/Guildwars2/
 */
class DataSource {
    companion object {
        fun createDataSet(): ArrayList<Card> {
            val list = ArrayList<Card>()
            list.add(
                Card(
                    "[Concept Art] Zephyrite Aspect Capes",
                    "These looks good and I could see them being added to the game!",
                    "https://preview.redd.it/griy3rk6urw41.jpg?width=640&crop=smart&auto=webp&s=2bad71340cfd3fe8959d4c7d6647728c02aca139",
                    "u/Makarow"
                )
            )
            list.add(
                Card(
                    "[Concept Art] Anti-Riot Legion Tower Shields",
                    "These are amazing!",
                    "https://preview.redd.it/6agircy8vrw41.png?width=640&crop=smart&auto=webp&s=d0b3d583af1116c3683e218456dda0e04279f7b7",
                    "u/Makarow"
                )
            )

            list.add(
                Card(
                    "Finally unlocked my Griffon.",
                    "Took me the entire weekend but it was all worth it.",
                    "https://preview.redd.it/rvrc426a2pw41.jpg?width=960&crop=smart&auto=webp&s=8564d318f61a5b73c094b05a0a586215f782be32",
                    "u/HolyBajezus"
                )
            )
            list.add(
                Card(
                    "Guild Wars 2 is all about cosmetics and open world.. Right?",
                    "Just passing by to remind you of the best boss event.",
                    "https://external-preview.redd.it/vtNolurcnKlnz7z8bAw-QB0BGs4QMnKw2EtFBOQPh3w.jpg?width=640&crop=smart&auto=webp&s=9dd27c4f77bf9485e41f8fdde5a27839bcd5f7ef",
                    "E/Mo"
                )
            )
            list.add(
                Card(
                    "I had to say goodbye to my senior kitty this weekend.",
                    "Now she'll be on adventures with me all across Tyria while I craft Chuka and Champawat in her honor.",
                    "https://preview.redd.it/l5hdqhypfiw41.jpg?width=640&crop=smart&auto=webp&s=fbc52e69d7178cf7335fbbe738897e6f1421484b",
                    "u/richelle"
                )
            )
            list.add(
                Card(
                    "He would have been such a great CM boss or Group Content",
                    "Anet should recycle old story encounters and instances as strikes or raid or fractals, just sprinkle in some extra mechanics.",
                    "https://preview.redd.it/7bc1klg5rww41.jpg?width=640&crop=smart&auto=webp&s=a2c0e304a82fafc76366a04f947c6d9495f7b980",
                    "u/GrandpaMaterial"
                )
            )
            list.add(
                Card(
                    "I finally joined the Fashion Wars",
                    "Took me long enough.",
                    "https://preview.redd.it/t4hzv5nsqpw41.jpg?width=640&crop=smart&auto=webp&s=95d7cfced5a0bc561ba227d9e0f85f031708df5c",
                    "u/firestorm_06"
                )
            )
            return list
        }
    }
}
