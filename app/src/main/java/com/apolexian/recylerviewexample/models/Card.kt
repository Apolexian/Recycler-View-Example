package com.apolexian.recylerviewexample.models

/**
 * Stores the required information about a card that is to be shown in the recycler view,
 * the [imgUrl] is the path to the image and in this case uses a network image. The [username]
 * tag is used to display who the card (can be though of as a post) is written by, however no user
 * class is implemented as the cards come from dummy data.
 */
data class Card(
    var titleText: String,
    var bodyText: String,
    var imgUrl: String,
    var username: String
) {

}