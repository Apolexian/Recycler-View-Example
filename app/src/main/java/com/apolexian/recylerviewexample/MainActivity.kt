package com.apolexian.recylerviewexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var cardAdapter: CardRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecyclerView()
        initDataSet()
    }

    /** Loads the hard coded data set - in real use apply some sort of business logic to load
     * data from the db
     */
    private fun initDataSet() {
        val data = DataSource.createDataSet()
        cardAdapter.submitCardList(data)
    }

    /** Uses the adapter to load the recycler view and adds padding to top using the decoration */
    private fun initRecyclerView() {
        recycler_view.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            val topDecoration = TopDecoration(25)
            addItemDecoration(topDecoration)
            cardAdapter = CardRecyclerAdapter()
            adapter = cardAdapter
        }
    }
}
